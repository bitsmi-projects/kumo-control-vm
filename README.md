# KUMO CONTROL VM

## Table of Contents

* [VM Startup](#markdown-header-vm-startup)
	* [(Optional) - Create a custom RSA key](#markdown-header-optional-create-a-custom-rsa-key)
	* [(Optional) - Disable default account](#markdown-header-optional-disable-default-account)
	* [Vagrant VM command summary](#markdown-header-vagrant-vm-command-summary)
* [Setup](#markdown-header-setup)		


## VM Startup

When the VM starts for first time with `vagrant up` command, it will perform the provision process described in **Ansible playbooks** located in `provision` folder.

Provision process creates an admin user account named **kumo_admin**. This user is a **sudoer** account created to group all kumo platform related tasks and permissions.
To enable it after VM first provision, stop VM with `vagrant halt` command and edit `Vagrantfile`. The following lines must be uncommented:

```
config.ssh.username = "kumo_admin"
config.ssh.private_key_path = ["id_rsa"]
```

This will enable user `kumo_admin` to logging in through SSH using default RSA key located in `id_rsa` file. **This file is not protected by a password**.

### (Optional) - Create a custom RSA key

At the moment, Vagrant RSA key only supports PEM format private keys, so `-m PEM` flag is mandatory.

```sh
ssh-keygen -m PEM -t rsa
```

It will generate 2 files: `~/.ssh/id_rsa` with the **private key** and `~/.ssh/id_rsa.pub` with the **public key**.

Copy the **public key** inside `provision` folder and the **private key** at VM root folder replacing default id_rsa file. 
Run `vagrant provision` command to copy the public key inside the VM. 

**NOTE:** On Windows systems, ssh client may not recognize private key's file as a secure source if it have too much permissions, 
E.G. if all users in the system have read / write permissions on it. To avoid these errors, ensure that only current Windows user figures in file's security policies
(Right button on file > Properties > Security)

On windows, SSH connection can be debugged with the following command (^ character is used to escape line breaks in multiple line commands):

```sh
ssh kumo_admin@127.0.0.1 -p 2222 -o LogLevel=DEBUG ^
-o Compression=yes -o DSAAuthentication=yes ^
-o IdentitiesOnly=yes -o StrictHostKeyChecking=no ^
-o UserKnownHostsFile=/dev/null -i id_rsa
```

### (Optional) - Disable default account

Although `kumo_admin` is the main user account designed to perform and execute all management tasks, Vagrant's default user `vagrant` it's still enabled and 
keeps all privileges of a sudoer account. It can be disabled or removed executing one of the following commands:

**Expire Account** 

It will disallow user from logging in from any source, including ssh

```sh
sudo usermod --expiredate 1 vagrant
```

Account can be reenabled setting expiration date to never o a specific date

```sh
# Set expiration to never
sudo usermod --expiredate "" vagrant

# Set expiration to an specific date formatted as YYYY-MM-DD
sudo usermod --expiredate "2020-12-31" vagrant
```

**Delete Account**

CAUTION! Deleted account cannot be restored

```sh
sudo deluser --remove-home vagrant

sudo groupdel vagrant
```

### Vagrant VM command summary

* `vagrant up`: Start VM. On first start-up, Vagrant will run the VM provision
* `vagrant halt`: Stop VM
* `vagrant reload`: Restart VM
* `vagrant provision`: Update VM provision after a change on **Vagrantfile**
* `vagrant ssh`: SSH login to VM with the default `vagrant` user or the one configured in `Vagrantfile`'s `config.ssh.username` property.

## Setup

#### Included Software

* Ansible 2.8.2

#### Network configuration

* **VM IP:** `172.16.0.11`. May be modified by a value inside any of private network ranges: 
	* 192.168.0.0 to 192.168.255.255 
	* 172.16.0.0 to 172.31.255.255 
	* 10.0.0.0 to 10.255.255.255
	
#### VM Folders
	
* `/home/kumo_admin`: **kumo_admin** user's home folder
* `/vagrant`: Shared folder corresponding to host's folder where **Vagrantfile** is located	
	